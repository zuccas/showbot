
var correct, incorrect;
var answered = false;			// whether they have answered or not
var answer;						// the ID of the correct answer
var newQString = "?";
var nextQuestionID;
var	xmlDoc;						// will hold the XML dom

// create our data holder with some default elements
var data = new Object();
data['id'] = 1;
data['lvl'] = "e";

function init()
{
	// load the question
	loadXMLData();
	parseQueryData();
	
	// load the correct/incorrect images
	correct = img = new Image();
	correct.width = 50;
	correct.height = 50;
	correct.src = "/assets/correct.png";
	
	incorrect = img = new Image();
	incorrect.width = 50;
	incorrect.height = 50;
	incorrect.src = "/assets/incorrect.png";
	
	setQuestion();
	displayQuestion();
	
	beginListening();
}

function parseQueryData()
{
	// parse the query string
	var qString = location.search;
	
	// remove the leading "?"
	qString = qString.substring(1);

	var pairs = qString.split("&"); 
	for( s in pairs )
	{
		var key =	pairs[s].substring( 0, pairs[s].indexOf("=") );
		var value =	pairs[s].substring( pairs[s].indexOf("=")+1 );
		data[key] = value;
		
		// copy the pairs into the new query string, but don't add the correct
		// count until after they have answered.
		if ( key != "correct" )
		{
		
			// copy the data to the new query string, except increment the "next" value
			if ( key == "pos" )
			{
				var nextvalue = String.fromCharCode( value.charCodeAt(0) + 1 );
				newQString += key + "=" + nextvalue + "&";
				nextQuestionID = nextvalue;
			}
			else
			{
				newQString += key + "=" + value + "&";
			}
		}
	}
	
	// determine the question Number
	var id = data['pos'].charCodeAt(0) - ("a").charCodeAt( 0 ) + 1;
	data['id'] = id;
	
	data['qid'] = data[ data['pos'] ];
	
}

function loadXMLData()
{
	xmlhttp=new XMLHttpRequest();
	xmlhttp.open("GET","./trivia.xml",false);
	xmlhttp.send();

	// apparently we don't need to wait for a loaded signal
	parser=new DOMParser();
	xmlDoc=parser.parseFromString(xmlhttp.responseText ,"text/xml");
}

function setQuestion()
{
	
	var itemNodes = xmlDoc.getElementsByTagName("item");
		
	for( var i =0; i<itemNodes.length; i++ )
	{
		// if the question is of the required level
		var thisItem = itemNodes[i];
		var itemLevel = thisItem.getAttribute("age");
		if ( itemLevel == "all" || itemLevel== data['level'] )
		{
			if ( i == data['qid'] )
			{
				var qNode = getFirstChild( thisItem );
				data['question'] = qNode.textContent;
				
				var optNodes = thisItem.getElementsByTagName("option");
				
				for( var j=1; j<=optNodes.length; j++ )
				{
					var thisOpt = optNodes[j-1];
					
					data[ 'option' + j ] = thisOpt.textContent;
					
					if ( thisOpt.getAttribute( "correct" ) == "correct" )
						answer = j;
				}
				
			}	
		}
	
	}
}

//check if the first node is an element node
function getFirstChild(n)
{
	x=n.firstChild;
	while (x.nodeType!=1)
  	{
  		x=x.nextSibling;
  	}
	return x;
}

function beginListening()
{
	// for some reason, the image does not react as part of the div, so add
	// each image separately.
	var div = document.getElementById('option1');
	div.addEventListener( "touchend", checkAnswer );
	div = document.getElementById('option2');
	div.addEventListener( "touchend", checkAnswer );
	div = document.getElementById('option3');
	div.addEventListener( "touchend", checkAnswer );
	div = document.getElementById('option4');
	div.addEventListener( "touchend", checkAnswer );
	div = document.getElementById('option5');
	div.addEventListener( "touchend", checkAnswer );
	
	// now the circle images
	var button_image = document.getElementById('opt1');
	button_image.addEventListener( "touchend", checkAnswer );
	button_image = document.getElementById('opt2');
	button_image.addEventListener( "touchend", checkAnswer );
	button_image = document.getElementById('opt3');
	button_image.addEventListener( "touchend", checkAnswer );
	button_image = document.getElementById('opt4');
	button_image.addEventListener( "touchend", checkAnswer );
	button_image = document.getElementById('opt5');
	button_image.addEventListener( "touchend", checkAnswer );

}

function checkAnswer( e )
{

	// alert( e ); // e is the element, not the event
	
	// rather than turn off all of the listening, let's just
	// throw a global flag.
	if ( answered )
		return;
	else
		answered = true;
	
	var selected = e.target.id;		// something like "option1" or "opt";
	var id = selected.charAt( selected.length - 1); // the last character
		
	// get the "option" button so we can change it to right or wrong
	var selOpt = document.getElementById( "opt" + id );
	
	var result;
	
	if ( id == answer )
	{
		selOpt.src = correct.src;	
		result = document.getElementById( "correct" );
		newQString += "correct=" + ( new Number( data['correct'] ) + 1);
	}
	else
	{
		selOpt.src = incorrect.src;
		
		var correctOpt = document.getElementById( "opt" + answer );
		correctOpt.src = correct.src;
		result = document.getElementById( "incorrect" );
		newQString += "correct=" + data['correct'];
	}
	
	result.style.visibility = "visible";
	
	// and the next button
	var next = document.getElementById( "next" );
	next.style.visibility = "visible";
	next.addEventListener( "touchend", goNext );
}

function displayQuestion()
{

	var question = 	document.getElementById("question");
	question.innerHTML = data['id'] + ". " + data['question']; 
	
	for( var i=1; i<=5; i++ )
	{
		var opID = "op" + i;
		var opt = document.getElementById( opID );
		opt.innerHTML = data['option' + i ];
	}
}

function goNext()
{
	// if there is a next question listed, go there. If not, were are done.
	
	if ( data[nextQuestionID] == undefined )
	{
		window.location.replace("./end.html" + newQString);
	}
	else
	{	
		window.location.replace("./question.html" + newQString);
	}
}

window.addEventListener('load', init );
