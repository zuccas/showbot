/*
 * External Dependencies
 *	Global vars - 
 *	HTML Elements	<canvas id="board" ...					
 */
 
function Board()
{
	this.canvas;
	this.context;

	this.height		= 760;
	this.width		= 760;
	this.margin		= 24;
	this.field		= this.width - 2*(this.margin);
	this.stroke		= 10;
	
	// each grid is (width - 2*margin )/3 wide, ignoring the stroked lines
	this.grid = Math.floor( (this.width - 2*this.margin)/3 );

	// the box accounts for the stroked lines
	this.box = Math.floor( (this.width - 2*this.margin)/3 ) - this.stroke;

	this.draw		= Board_draw;
	this.makePiece	= Board_makePiece;
	this.placePiece	= Board_placePiece;
	this.mapClick	= Board_mapClick;
	
	this.draw();
	this.X = this.makePiece( "X", "green" );
	this.O = this.makePiece( "O", "yellow" );
	this.blank = this.makePiece( "!", "blank" );	// a bit wasteful, but ...
	
}

function Board_draw()
{
	this.canvas = document.getElementById('board');
	
	// black out the canvas
	this.context = this. canvas.getContext('2d');
	this.context.fillStyle = 'black';
	this.context.fillRect(0,0, this.width, this.height);
	
	// draw the vertical lines
	var x = this.margin + Math.floor( this.field/3 ) - this.stroke/2;
	var y = this.margin;
	
	this.context.fillStyle='white';
	this.context.fillRect( x, y, this.stroke, this.field);
	this.context.fillRect( y, x, this.field, this.stroke);
	
	// draw the horizontal lines
	x = this.margin + Math.floor( 2*this.field/3 ) - this.stroke/2;
	this.context.fillRect( x, y, this.stroke, this.field);
	this.context.fillRect( y, x, this.field, this.stroke);
}

function Board_makePiece( id, color )
{
	square = document.createElement('canvas');
	square.width = this.box;
	square.height = this.box;
	
	ctxt = square.getContext("2d");
	
	// fill in with black (could transparent work)
	ctxt.fillStyle='#000000';
	ctxt.fillRect(0, 0, this.box, this.box);
		
	
	// hardcoded font size
	ctxt.font = "bold 225px 'Arial', san-serif";
	ctxt.textAlign = "center";
	
	// hardcoded offset
	ctxt.fillStyle= color;
	ctxt.fillText( id, this.box/2, this.box - 30 );
		
	return square;
}

function Board_placePiece( row, col, player )
{
	var pid = this.blank;
	if (player == HUMAN ) pid = this.X;
	if (player == ROBOT ) pid = this.O;
	
	// jump over the margins ( + margin )
	// move to grid position ( + x*grid )
	// jump over the 1/2 stroke in this box ( + stroke/2 )
	var xpos = this.margin + row*this.grid + this.stroke/2;
	var ypos = this.margin + col*this.grid + this.stroke/2;
	
	this.context.drawImage( pid, xpos, ypos, this.box, this.box );
}

function Board_mapClick( x, y )
{
	// the click is relative to the window, not the canvas
	// currently, top=12 and left=12
	// how do we find this programatically?
	
	// console.log( "ORIG: x=" + x + ", y=" + y );
	
	// convert to click position to grid indices.
	// remove the margins and divide by the grid size
	x = Math.floor( (x - 6 - this.margin)/this.grid );
	y = Math.floor( (y - 6 - this.margin)/this.grid );

	// console.log( "MAP: x=" + x + ", y=" + y );
	
	var idx = x + 3*y;
	return idx;
}