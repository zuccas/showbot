/*
 * External Dependencies
 *	global vars - HUMAN, ROBOT, BLANK
 *	
 */
 
function TicTacToe()
{
	this.game = new Array( 0,0,0,0,0,0,0,0,0 );
	this.first_turn = HUMAN;
	
	this.combos = new Array( "012", "345", "678",	// horizontal
							 "036", "147", "258",	// vertical
							 "048", "246" )			// diagonal
	
	this.winCombo = "";
	
	this.takeTurn		= TicTacToe_takeTurn;
	this.isValidTurn	= TicTacToe_isValidTurn;
	this.reset			= TicTacToe_reset;
	this.checkWinner	= TicTacToe_checkWinner;
	this.checkTie		= TicTacToe_checkTie;
	this.getRobotMove	= TicTacToe_getRobotMove;

}

function TicTacToe_reset()
{
	// reset the game 
	this.game = new Array( 0,0,0,0,0,0,0,0,0 );
	this.winCombo = "";
	 
	// alternate between human first and computer first
	if ( this.first_turn == HUMAN )
	{
		this.first_turn = ROBOT;
		turnEventsOff();
		t = window.setInterval( "robotTurn()", 250 );
		
	}
	else
	{
		this.first_turn = HUMAN;
		turnEventsOn();
	}
}

function TicTacToe_getRobotMove()
{
	var move = -1;
	
	// look for a win
	for( var i=0; i<this.combos.length; i++ )
	{
		var a = this.combos[i].charAt(0);
		var b = this.combos[i].charAt(1);
		var c = this.combos[i].charAt(2);
		
		if ( this.game[a]==ROBOT && this.game[b]==ROBOT && this.game[c]==BLANK) move = c;
		if ( this.game[a]==ROBOT && this.game[c]==ROBOT && this.game[b]==BLANK) move = b;
		if ( this.game[b]==ROBOT && this.game[c]==ROBOT && this.game[a]==BLANK) move = a;
		
		if ( move != -1 ) return move;
	}
	
	// look for a block
	for( var i=0; i<this.combos.length; i++ )
	{
		var a = this.combos[i].charAt(0);
		var b = this.combos[i].charAt(1);
		var c = this.combos[i].charAt(2);
		
		if ( this.game[a]==HUMAN && this.game[b]==HUMAN && this.game[c]==BLANK) move = c;
		if ( this.game[a]==HUMAN && this.game[c]==HUMAN && this.game[b]==BLANK) move = b;
		if ( this.game[b]==HUMAN && this.game[c]==HUMAN && this.game[a]==BLANK) move = a;
		
		if ( move != -1 ) return move;
	}
	
	// The computer sometimes gets into a position like
	//	o _ _
	//  x o _
	//  _ _ x
	//
	// It should "go for the kill" in the upper right.
	if( this.game[4] == ROBOT )				// bot must have the center
	{
		for( var i=0; i<this.combos.length; i++ )
		{
			var a = this.combos[i].charAt(0);
			var b = this.combos[i].charAt(1);
			var c = this.combos[i].charAt(2);
			
			if ( b == 4 ) continue;		// don't check ones through the center
		
			if ( this.game[a]==ROBOT && this.game[b]==BLANK && this.game[c]==BLANK) move = c;
			if ( this.game[c]==ROBOT && this.game[b]==BLANK && this.game[a]==BLANK) move = a;
		}
		
		if ( move != -1 ) return move;
	}
		
	
	
	// Random play
	// If the center is open , take it 60% of the time
	if( this.game[4] == BLANK && Math.random() > .6 )
	{
		move = 4;		// 4 - CENTER
		return move;
	}
	
	// If a corner (space 0,2,6 or 8) is open, take it randomly
	var openCorners = "";
	if ( this.game[0] == BLANK ) openCorners += "0";
	if ( this.game[2] == BLANK ) openCorners += "2";
	if ( this.game[6] == BLANK ) openCorners += "6";
	if ( this.game[8] == BLANK ) openCorners += "8";
	
	var index;
	
	if( openCorners.length > 0 )
	{
		// should we take the move (80% of the time, do it)
		if ( Math.random() > .8 )
		{
			// now pick a random corner
			index = Math.floor( Math.random()*openCorners.length );
			move = openCorners.substring(index, index+1);
			// alert( "Open corners: " + openCorners + " taking: " + index + ":" +move );
			return move;
		}
	}
	
	// rather than randomly selecting a random edge/middle, just
	// choose randomly from what is left. That avoids the case where
	// we skip each category (center/corner/edge) and come up without
	// a valid move.
	
	var openSpaces = "";
	for( var i=0; i<9; i++)
	{
		if ( this.game[i] == BLANK ) openSpaces += i;
	}
	
	index = Math.floor( Math.random()*openSpaces.length );
	move = openSpaces.substring(index, index+1);
	
	// alert( "Open spaces: " + openSpaces + " taking: " + index + ":" + move );
	
	return move;
}

function TicTacToe_takeTurn( player_id, location_id)
{
	if ( this.isValidTurn( location_id ) )
	{
		this.game[location_id] = player_id;
	}
}

function TicTacToe_isValidTurn( n )
{
	return ( this.game[n] == 0 );
}

function TicTacToe_checkWinner( player_id )
{
	for( var i=0; i<this.combos.length; i++ )
	{
		var a = this.combos[i].charAt(0);
		var b = this.combos[i].charAt(1);
		var c = this.combos[i].charAt(2);
		
		var bWin = false;
		
		if (this.game[a] == player_id && this.game[a] == this.game[b] && this.game[b] == this.game[c] )
		{
			bWin = true;
			this.winCombo = this.combos[i];
			break;
		}
		
	}
	
	return bWin;
}

function TicTacToe_checkTie()
{
	var bTie = true;
	
	for( var i=0; i<9; i++ )
	{
		if ( this.game[i] == 0 )
		{
			bTie = false;
			break;
		}
	}
	
	return bTie;
}
